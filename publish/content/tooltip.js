function unused(v){
  return v;
}

const tooltip = {
    hideTooltipTimeOut : 1000,
    showTooltipTimeOut : 2500,
    xOffset : 20,
    yOffset : 0,
    area : Object,
    shadowArea : Object,
    tipLayer : Object,
    tipConnectorLayer : Object,
    tipPinLayer : Object,
    hideTimeoutId : '',
    showTimeoutId : '',
    opacityId : '',
    tooltipVisible : false,
    init : function(){
      window.tipLayer = document.createElement('div');
      tipLayer.id = 'toolTip';
      document.getElementsByTagName('body')[0].appendChild(tipLayer);
      tipLayer.className="TableContent Documentation";
      tipLayer.style.top = '0';
      tipLayer.style.visibility = 'hidden';
      tipLayer.style.zIndex=5;
      tipLayer.onmouseover= function onmouseover(event) {
        unused(event);
        if (window.showTimeoutId) {
          clearTimeout(showTimeoutId);
        }
        if (window.hideTimeoutId) {
          clearTimeout(hideTimeoutId);
        }
      };
      tipLayer.onmouseout= function onmouseout(event) {
        unused(event);
        tooltip.hide();
      };
      {
        // tip connector
        window.tipConnectorLayer = document.createElement('div');
        tipConnectorLayer.id = 'toolTipConnector';
        document.getElementsByTagName('body')[0].appendChild(tipConnectorLayer);
        tipConnectorLayer.className="Documentation";
        tipConnectorLayer.style.top = '0';
        tipConnectorLayer.style.visibility = 'hidden';
        tipConnectorLayer.style.zIndex=2; // index = 2, below indicator's index = 3
        tipConnectorLayer.onmouseover = tipLayer.onmouseover;
        tipConnectorLayer.onmouseout = tipLayer.onmouseout;
      }
      {
        // tip pin
        window.tipPinLayer = document.createElement('div');
        tipPinLayer.id = 'toolTipPin';
        document.getElementsByTagName('body')[0].appendChild(tipPinLayer);
        tipPinLayer.className="Documentation";
        tipPinLayer.style.top = '0';
        tipPinLayer.style.visibility = 'hidden';
        tipPinLayer.style.zIndex=2; // index = 2, below indicator's index = 3
        tipPinLayer.onmouseover = tipLayer.onmouseover;
        tipPinLayer.onmouseout = tipLayer.onmouseout;
      }
      window.shadowArea = document.createElement('div');
      shadowArea.id = 'shadowArea';
      shadowArea.onmouseover= tipLayer.onmouseover;
      shadowArea.onmouseout = tipLayer.onmouseout;
      document.getElementsByTagName('body')[0].appendChild(shadowArea);
    },
    show : function(area){
      this.tooltipVisible = true;
      if (window.hideTimeoutId) {
        clearTimeout(hideTimeoutId);
      }
      if (window.opacityId) {
        clearTimeout(opacityId);
      }
      this.area = area;
      area.removeAttribute('title');
      this.updateShadowArea(area);
      tipLayer.style.visibility = 'visible';
      tipLayer.style.opacity = '.1';
      tipLayer.style.zIndex=5;
      tipLayer.innerHTML = area.getAttribute('docContent')
        .replace(/&gt;/g, '>').replace(/&lt;/g, '<')
        .replace(/&\u200blt;/g, '&lt;').replace(/&\u200bgt;/g, '&gt;') // replace the &{zero-length-space}lt; to &lt;
        .replace(/&quot;/g, '"');
      if ($) {
        $('font[color="rgb(68, 68, 68)"]')
          .attr('color',null)
          .attr('face',null)
          .attr('style',null)
          .attr('size',null)
          .attr('class','md-code');
        $('a[href^="http://localhost/aip.html"]').attr('href',null);//.css('cursor','pointer');
      }
      tipConnectorLayer.style.visibility = 'visible';
      tipConnectorLayer.style.opacity = '.1';
      tipConnectorLayer.style.zIndex=5;
      tipPinLayer.style.visibility = 'visible';
      tipPinLayer.style.opacity = '.1';
      tipPinLayer.style.zIndex=5;
      this.moveTo(area);
      this.fade(10);
    },
    updateShadowArea : function(area){
      const areaCoordinate = area.coords.split(",");
      const topRightX = areaCoordinate[2]*1;
      const topRightY = areaCoordinate[1]*1;
      const width = this.xOffset;
      const height = areaCoordinate[3]*1 - topRightY;
      const N = (document.all) ? 0 : 1;
      const diagram = document.getElementById("diagram");
      if (N) {
        shadowArea.style.left = findPosX(diagram) + topRightX;
        shadowArea.style.top = findPosY(diagram) + topRightY;
      } else {
        shadowArea.style.posLeft = findPosX(diagram) + topRightX;
        shadowArea.style.posTop = findPosY(diagram) + topRightY;
      }
      shadowArea.style.width = width;
      shadowArea.style.height = height;
    },
    fade : function(opac) {
      const passed = parseInt(opac);
      const newOpac = parseInt(passed+5);
      if ( newOpac < 90 ) {
        tipLayer.style.opacity = '.'+newOpac;
        tipLayer.style.filter = "alpha(opacity:"+newOpac+")";
        tipConnectorLayer.style.opacity = '.'+newOpac;
        tipConnectorLayer.style.filter = "alpha(opacity:"+newOpac+")";
        tipPinLayer.style.opacity = '.'+newOpac;
        tipPinLayer.style.filter = "alpha(opacity:"+newOpac+")";
        window.opacityId = window.setTimeout("tooltip.fade('"+newOpac+"')", 40);
      }else{
        tipLayer.style.opacity = '.90';
        tipLayer.style.filter = "alpha(opacity:90)";
        tipConnectorLayer.style.opacity = '.90';
        tipConnectorLayer.style.filter = "alpha(opacity:90)";
        tipPinLayer.style.opacity = '.90';
        tipPinLayer.style.filter = "alpha(opacity:90)";
      }
    },
    moveTo : function(area) {
      const diagram = document.getElementById("diagram");
      const lDiagramX = findPosX(diagram);
      const lDiagramY = findPosY(diagram);
      let lTipLayerX = lDiagramX;
      let lTipLayerY = lDiagramY + diagram.height;
      lTipLayerX += getScrollLeft();
      const lAreaCoordinate = area.coords.split(",");
      //var lAreaTop = lAreaCoordinate[1]*1 + lDiagramY;
      const lAreaBottom = lAreaCoordinate[3]*1 + lDiagramY;
      const lAreaX1 = lAreaCoordinate[0]*1 + lDiagramX;
      const lAreaX2 = lAreaCoordinate[2]*1 + lDiagramX;
      let lScreenBottom = getScrollTop()+getClientHeight() - tipLayer.offsetHeight; // no height is set in layer. Just try to - 300
      if (lTipLayerY > lScreenBottom) {
        if (lScreenBottom < lAreaBottom) {
          // don't overlap with the area
          lScreenBottom = lAreaBottom;
        }
        lTipLayerY = lScreenBottom;
      }
      const N = (document.all) ? 0 : 1;
      if (N) {
        tipLayer.style.left = lTipLayerX;
        tipLayer.style.top = lTipLayerY;
        tipPinLayer.style.left = lAreaX1;
        tipPinLayer.style.top = lAreaBottom;
        tipConnectorLayer.style.left = lAreaX1+((lAreaX2-lAreaX1)/2); // -2 for connector's width/2
        tipConnectorLayer.style.top = lAreaBottom+6; // +6 for pin's height+borderHeight
      }else{
        tipLayer.style.posLeft = lTipLayerX;
        tipLayer.style.posTop = lTipLayerY;
        tipPinLayer.style.posLeft = lAreaX1;
        tipPinLayer.style.posTop = lAreaBottom;
        tipConnectorLayer.style.posLeft = lAreaX1+((lAreaX2-lAreaX1)/2); // -2 for connector's width/2
        tipConnectorLayer.style.posTop = lAreaBottom+6; // +6 for pin's height+borderHeight
      }
      tipLayer.style.width = getWidth(lTipLayerX);
      tipPinLayer.style.width = lAreaX2-lAreaX1;
      if (lTipLayerY > lAreaBottom+6) {
        tipConnectorLayer.style.height = lTipLayerY-lAreaBottom-6;
      }else{
        tipPinLayer.style.visibility = 'hidden';
        tipConnectorLayer.style.visibility = 'hidden';
      }
    },
    hide : function() {
      window.hideTimeoutId = window.setTimeout(function() {
        this.tooltipVisible = false;
        if (window.showTimeoutId) {
          clearTimeout(showTimeoutId);
        }
        if (window.hideTimeoutId) {
          clearTimeout(hideTimeoutId);
        }
        if (window.opacityId) {
          clearTimeout(opacityId);
        }
        tipLayer.style.visibility = 'hidden';
        tipConnectorLayer.style.visibility = 'hidden';
        tipPinLayer.style.visibility = 'hidden';
      }, this.hideTooltipTimeOut);
    }
};

unused(initPage);
function initPage(){
  tooltip.init();
}

unused(showTooltip);
function showTooltip(area) {
  if (tooltip.tooltipVisible) {
    if (window.showTimeoutId) {
      clearTimeout(showTimeoutId);
    }
    if (window.hideTimeoutId) {
      clearTimeout(hideTimeoutId);
    }
    window.showTimeoutId = window.setTimeout(function() {
      if (window.showTimeoutId) {
        clearTimeout(showTimeoutId);
      }
      tooltip.show(area);
    }, tooltip.showTooltipTimeOut);
  }
  else {
    tooltip.show(area);
  }
}

unused(hideTooltip);
function hideTooltip(){
  tooltip.hide();
}

function getWidth(aLeft) {
  return getClientWidth() + getScrollLeft() - aLeft - 20;
}

function getClientWidth() {
  return filterResults (
      window.innerWidth ? window.innerWidth : 0,
          document.documentElement ? document.documentElement.clientWidth : 0,
              document.body ? document.body.clientWidth : 0
  );
}
function getClientHeight() {
  return filterResults (
      window.innerHeight ? window.innerHeight : 0,
          document.documentElement ? document.documentElement.clientHeight : 0,
              document.body ? document.body.clientHeight : 0
  );
}
function getScrollLeft() {
  return filterResults (
      window.pageXOffset ? window.pageXOffset : 0,
          document.documentElement ? document.documentElement.scrollLeft : 0,
              document.body ? document.body.scrollLeft : 0
  );
}
function getScrollTop() {
  return filterResults (
      window.pageYOffset ? window.pageYOffset : 0,
          document.documentElement ? document.documentElement.scrollTop : 0,
              document.body ? document.body.scrollTop : 0
  );
}
function filterResults(aWin, aDoc, aBody) {
  let aResult = aWin ? aWin : 0;
  if (aDoc && (!aResult || (aResult > aDoc)))
    aResult = aDoc;
  return aBody && (!aResult || (aResult > aBody)) ? aBody : aResult;
}