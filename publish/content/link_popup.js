let xForRefIcon = 0;
let yForRefIcon = 0;

let xForSubDiagramIcon = 0;
let yForSubDiagramIcon = 0;

let xForTransIcon = 0;
let yForTransIcon = 0;

let fileLinks = [];
let folderLinks = [];
let urlLinks = [];
let diagramLinks = [];
let shapeLinks = [];
let subdiagramLinks = [];
let fromTransitorLinks = [];
let toTransitorLinks = [];
let modelElementLinks = [];


let vpLinkNameToggle;

const PROJECT='keyprism3';
//const BITBUCKET_PRJ='keyprism3';
//const BITBUCKET_REPO='keyprism3';
const BITBUCKET_MASTER='https://bitbucket.org/'+BITBUCKET_PRJ+'/'+BITBUCKET_REPO+'/src/master/';
const PROJECT_REFS='http%3A//mrobbinsassoc.com/projects/'+PROJECT+'/vp/publish/references/';
const GESHI='http://mrobbinsassoc.com/tools/geshi/geshi-url-lang-title.php?url=';
const MD_IMGS='http://mrobbinsassoc.com/images/icons/md/';

function unused(v){
  return v;
}

unused(showDefaultReferenceIcon);
function showDefaultReferenceIcon(modelValues, objectId) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeX = xyValueArray[0]*1;
      const shapeY = xyValueArray[1]*1;
      const x = shapeX + xOffset*1;
      const y = shapeY + yOffset*1 - 13;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const referenceIconLayer = document.getElementById(objectId);
      const N = (document.all) ? 0 : 1;
      if (N) {
        referenceIconLayer.style.left = x - 3;
        referenceIconLayer.style.top = y + h;
      }else{
        referenceIconLayer.style.posLeft = x - 3;
        referenceIconLayer.style.posTop = y + h;
      }
      referenceIconLayer.style.visibility="visible"
    //}
  }
}

unused(showReferenceIcon);
function showReferenceIcon(modelValues) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeX = xyValueArray[0]*1;
      const shapeY = xyValueArray[1]*1;
      const x = shapeX + xOffset*1;
      const y = shapeY + yOffset*1 - 13;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const referenceIconLayer = document.getElementById("referenceIconLayer");
      const N = (document.all) ? 0 : 1;
      if (N) {
        referenceIconLayer.style.left = x - 3;
        referenceIconLayer.style.top = y + h;
      } else {
        referenceIconLayer.style.posLeft = x - 3;
        referenceIconLayer.style.posTop = y + h;
      }
      referenceIconLayer.style.visibility="visible"
    //}
  }
}

unused(hideReferenceIcon);
function hideReferenceIcon() {
  const referenceIconLayer = document.getElementById("referenceIconLayer");
  if (referenceIconLayer !== null) {
    referenceIconLayer.style.visibility="hidden"
  }
}

unused(showDefaultSubdiagramIcon);
function showDefaultSubdiagramIcon(modelValues, objectId) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeRightX = xyValueArray[2]*1;
      const shapeRightY = xyValueArray[1]*1;
      const x = shapeRightX + xOffset*1 - 10;
      const y = shapeRightY + yOffset*1 - 13;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const subdiagramIconLayer = document.getElementById(objectId);
      const N = (document.all) ? 0 : 1;
      if (N) {
        subdiagramIconLayer.style.left = x - 3;
        subdiagramIconLayer.style.top = y + h;
      } else {
        subdiagramIconLayer.style.posLeft = x - 3;
        subdiagramIconLayer.style.posTop = y + h;
      }
      subdiagramIconLayer.style.visibility="visible"
    //}
  }
}

unused(showSubdiagramIcon);
function showSubdiagramIcon(modelValues) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeRightX = xyValueArray[2]*1;
      const shapeRightY = xyValueArray[1]*1;
      const x = shapeRightX + xOffset*1 - 10;
      const y = shapeRightY + yOffset*1 - 13;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const subdiagramIconLayer = document.getElementById("subdiagramIconLayer");
      const N = (document.all) ? 0 : 1;
      if (N) {
        subdiagramIconLayer.style.left = x - 3;
        subdiagramIconLayer.style.top = y + h;
      } else {
        subdiagramIconLayer.style.posLeft = x - 3;
        subdiagramIconLayer.style.posTop = y + h;
      }
      subdiagramIconLayer.style.visibility="visible"
    //}
  }
}

unused(hideSubdiagramIcon);
function hideSubdiagramIcon() {
  const subdiagramIconLayer = document.getElementById("subdiagramIconLayer");
  if (subdiagramIconLayer !== null) {
    subdiagramIconLayer.style.visibility="hidden"
  }
}

unused(showDefaultTransitorIcon);
function showDefaultTransitorIcon(modelValues, objectId) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeRightX = xyValueArray[2]*1;
      const shapeRightY = xyValueArray[1]*1;
      const x = shapeRightX + xOffset*1 - 10;
      const y = shapeRightY + yOffset*1;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const transitorIconLayer = document.getElementById(objectId);
      const N = (document.all) ? 0 : 1;
      if (N) {
        transitorIconLayer.style.left = x - 3;
        transitorIconLayer.style.top = y + h;
      } else {
        transitorIconLayer.style.posLeft = x - 3;
        transitorIconLayer.style.posTop = y + h;
      }
      transitorIconLayer.style.visibility="visible"
    //}
  }
}

unused(showTransitorIcon);
function showTransitorIcon(modelValues) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeRightX = xyValueArray[2]*1;
      const shapeRightY = xyValueArray[1]*1;
      const x = shapeRightX + xOffset*1 - 10;
      const y = shapeRightY + yOffset*1;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const transitorIconLayer = document.getElementById("transitorIconLayer");
      const N = (document.all) ? 0 : 1;
      if (N) {
        transitorIconLayer.style.left = x - 3;
        transitorIconLayer.style.top = y + h;
      } else {
        transitorIconLayer.style.posLeft = x - 3;
        transitorIconLayer.style.posTop = y + h;
      }
      transitorIconLayer.style.visibility="visible"
    //}
  }
}

unused(hideTransitorIcon);
function hideTransitorIcon() {
  const transitorIconLayer = document.getElementById("transitorIconLayer");
  if (transitorIconLayer !== null) {
    transitorIconLayer.style.visibility="hidden"
  }
}

unused(showDefaultDocumentationIcon);
function showDefaultDocumentationIcon(modelValues, objectId) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeX = xyValueArray[0]*1;
      const shapeY = xyValueArray[1]*1;
      const x = shapeX + xOffset*1;
      const y = shapeY + yOffset*1;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const documentationIconLayer = document.getElementById(objectId);
      const N = (document.all) ? 0 : 1;
      if (N) {
        documentationIconLayer.style.left = x - 3;
        documentationIconLayer.style.top = y + h;
      } else {
        documentationIconLayer.style.posLeft = x - 3;
        documentationIconLayer.style.posTop = y + h;
      }
      documentationIconLayer.style.visibility="visible"
    //}
  }
}

unused(showDocumentationIcon);
function showDocumentationIcon(modelValues) {
  if (modelValues !== ''){
    const xyValueArray = modelValues.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      const shapeX = xyValueArray[0]*1;
      const shapeY = xyValueArray[1]*1;
      const x = shapeX + xOffset*1;
      const y = shapeY + yOffset*1;// WAS const y = shapeRightY + yOffset*1;
      const h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //const url = xyValueArray[4];
      const documentationIconLayer = document.getElementById("documentationIconLayer");
      const N = (document.all) ? 0 : 1;
      if (N) {
        documentationIconLayer.style.left = x - 3;
        documentationIconLayer.style.top = y + h;
      } else {
        documentationIconLayer.style.posLeft = x - 3;
        documentationIconLayer.style.posTop = y + h;
      }
      documentationIconLayer.style.visibility="visible"
     //}
  }
}

unused(hideDocumentationIcon);
function hideDocumentationIcon() {
  const documentationIconLayer = document.getElementById("documentationIconLayer");
  if (documentationIconLayer !== null) {
    documentationIconLayer.style.visibility="hidden"
  }
}

unused(storeReferenceAndSubdiagramInfos);
function storeReferenceAndSubdiagramInfos(coords, fileRefs, folderRefs, urlRefs, diagramRefs, shapeRefs, subdiagrams, modelElementRefs, fromTransitors, toTransitors) {
  if (coords !== ''){
    const xyValueArray = coords.split(",");
    //const shapeWidth = xyValueArray[2]*1 - xyValueArray[0]*1;
    //if (shapeWidth > 24){
      fileLinks = [];
      folderLinks = [];
      urlLinks = [];
      diagramLinks = [];
      shapeLinks = [];
      subdiagramLinks = [];
      modelElementLinks = [];
      fromTransitorLinks = [];
      toTransitorLinks = [];
      //const popup = document.getElementById("linkPopupMenuTable");
      for (let i = 0 ; i < fileRefs.length ; i++) {
        fileLinks[i] = fileRefs[i];
      }
      for (let i = 0 ; i < folderRefs.length ; i++) {
        folderLinks[i] = folderRefs[i];
      }
      for (let i = 0 ; i < urlRefs.length ; i++) {
        urlLinks[i] = urlRefs[i];
      }
      for (let j = 0 ; j < diagramRefs.length ; j++) {
        diagramLinks[j] = diagramRefs[j]
      }
      for (let j = 0 ; j < shapeRefs.length ; j++) {
        shapeLinks[j] = shapeRefs[j]
      }
      for (let j = 0 ; j < subdiagrams.length ; j++) {
        subdiagramLinks[j] = subdiagrams[j]
      }
      for (let j = 0 ; j < modelElementRefs.length ; j++) {
        modelElementLinks[j] = modelElementRefs[j]
      }
      for (let j = 0 ; j < fromTransitors.length ; j++) {
        fromTransitorLinks[j] = fromTransitors[j]
      }
      for (let j = 0 ; j < toTransitors.length ; j++) {
        toTransitorLinks[j] = toTransitors[j]
      }
      const diagram = document.getElementById("diagram");
      const xOffset = findPosX(diagram);
      const yOffset = findPosY(diagram);
      let shapeX = xyValueArray[0]*1;
      let shapeY = xyValueArray[1]*1;
      let x = shapeX + xOffset*1;
      let y = shapeY + yOffset*1 + 2;
      //let w = xyValueArray[2]*1 - xyValueArray[0]*1;
      let h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //let url = xyValueArray[4];
      xForRefIcon = x;
      yForRefIcon = y + h;
      shapeX = xyValueArray[2]*1;
      shapeY = xyValueArray[1]*1;
      x = shapeX + xOffset*1 - 12;
      y = shapeY + yOffset*1 + 2;
      //w = xyValueArray[2]*1 - xyValueArray[0]*1;
      h = xyValueArray[3]*1 - xyValueArray[1]*1;
      //url = xyValueArray[4];
      xForSubDiagramIcon = x;
      yForSubDiagramIcon = y + h;
      xForTransIcon = x;
      yForTransIcon = y + h + 12;
    //}
  }
}

function destinationExplhere(dest){
  // ../references/_tools`socialnitro-app`_;C.$explhere
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  const right=dest.substr('../references/'.length);
  let ra=right.split('`');
  ra=ra.reverse();
  let pfx='https://bitbucket.org/'+BITBUCKET_PRJ+'/';
  pfx=pfx+ra[1]+'/src/master';
  for (let x=2;x<ra.length;x++) {
    pfx+='/'+ra[x];
  }
  return pfx;
}

function destinationMd(dest){
  // ../references/RMF-_docs.md
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  const bb=BITBUCKET_MASTER;
  const f=dest.substr('../references/'.length);
  let ff=f.replace(/\.md/g,'').substr('RMF-'.length);
  ff=ff.replace(/-/g,'/');
  const path=ff+'/'+f;
  return bb+path;
}

function destinationHtml(dest){
  // ../references/xxx.html
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  const geshi=GESHI;
  const refPath=PROJECT_REFS;
  const f=dest.substr('../references/'.length);
  return geshi + refPath + f + '&lang=HTML5&title=' + encodeURIComponent(f);
}

function destinationJs(dest){
  // ../references/xxx.html
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  const geshi=GESHI;
  const refPath=PROJECT_REFS;
  const f=dest.substr('../references/'.length);
  return geshi + refPath + f + '&lang=javascript&title=' + encodeURIComponent(f);
}

function destinationCss(dest){
  // ../references/xxx.html
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  const geshi=GESHI;
  const refPath=PROJECT_REFS;
  const f=dest.substr('../references/'.length);
  return geshi + refPath + f + '&lang=css&title=' + encodeURIComponent(f);
}

function destinationFeature(dest){
  // ../references/xxx.html
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  const bb=BITBUCKET_MASTER;
  const f=dest.substr('../references/'.length);
  return bb + 'tests/chimp/features/' + f;
}

function destinationHook(dest){
  console.log(dest);
  const da=dest.split('.');
  const ext=da[da.length-1];
  // ../references/_tools`socialnitro-app`_;C.$explhere
  // https://bitbucket.org/socialteria/socialnitro-app/src/master/imports/api/lists/lists.tests.js
  if (dest.substr(0,'../references/'.length)==='../references/') {
    if (ext==='$explhere') {
      return destinationExplhere(dest);
    }
    if (ext==='md') {
      return destinationMd(dest);
    }
    if (ext==='html') {
      return destinationHtml(dest);
    }
    if (ext==='feature') {
      return destinationFeature(dest);
    }
    if (ext==='js'||ext==='jsx'||ext==='json') {
      return destinationJs(dest);
    }
    if (ext==='less'||ext==='css') {
      return destinationCss(dest);
    }
  }
  return dest;
}

function popupRowHtml(name, img) {
  let s='';
  s+='<div';
  s+=' style="';
  s+=' float: left;';
  s+=' width: 18px !important;';
  s+=' height: 18px !important;';
  if (img) {
    s+=' background-image:url('+img+') !important;';
    s+=" background-image:url('');";
    s+=" filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+img+"');";
    s+=' background-size:contain;';
  }
  s+='"';
  s+='>';
  s+='</div>';
  s+='&nbsp;';
  s+=name;
  return s;
}

function fileExt(name){
  let ext=name.split('.');
  ext=ext[ext.length-1];
  return ext;
}

function popupRowFileRefImage(name, url){
  unused(url);
  const extensions='bat css eslintrc feature gitignore html ini js json jsx less md sass scss txt vpj'.split(' ');
  let imageUrl='../images/icons/FileReference.png';
  const ext=fileExt(name);
  if (extensions.indexOf(ext)!==-1) {
    imageUrl=MD_IMGS+'_'+ext+'-16.png';
  }
  return imageUrl;
}

function createPopupRow(popup,data) { //name, url, img, cssMore, onclick
  const row = popup.insertRow(popup.rows.length);
  const imgPopupCell = row.insertCell(0);
  imgPopupCell.innerHTML=popupRowHtml(data.name, data.img);
  imgPopupCell.valign="middle";
  imgPopupCell.destination=data.url;
  imgPopupCell.className="PopupMenuRowDeselected";
  imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
  imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
  imgPopupCell.onclick = data.onclick;
}

function explhereNameToDir(name){
  // _tools`socialnitro-app`_;C.$explhere
  name=name.replace(/\.\$explhere$/,'');
  // _tools`socialnitro-app`_;C
  name=name.replace(/;/,'`');
  let a=name.split('`');
  a=a.reverse();
  a[0]=a[0]+':';
  return a.join('\\');
}

function notyDataError(text){
  const o={text:text};
  o.timeout=5000;
  o.layout='bottomRight';
  o.type='error';
  o.progressBar=true;
  //noinspection JSUnresolvedFunction
  noty(o);
}

function createExplhereRows(popup, name, url){
  const name0=name;
  const disk_path=explhereNameToDir(name0);
  //noinspection JSUnresolvedVariable
  const ini_data=window.DESKTOPINI[disk_path];
  if (ini_data) {
    if (ini_data.thought_id) {
      //noinspection JSUnresolvedVariable
      const tid=ini_data.webthought_id||ini_data.thought_id;
      const data={name:'WebBrain', url:url, img:MD_IMGS+'webbrain-16.png'};
      data.onclick= function onclick(event) {
        unused(event);
        //notyDataError(name);
        const msg={
          from:window.name
          ,action:'brainNav'
          ,thought_id:tid
        };
        top.postMessage(msg,'*');
        //window.open(destinationHook(this.destination));
      };
      createPopupRow(popup,data);
    }else{
      const data={name:'No thought_id', url:url, img:MD_IMGS+'error-16.png'};
      data.onclick=function onclick(event){
        unused(event);
        location.href='aip://slickedit/'+encodeURIComponent(disk_path+'\\Desktop.ini');
      };
      createPopupRow(popup,data);
    }
    //noinspection JSUnresolvedVariable
    if (ini_data.thought_uid) {
      const data={name:'Brain Local', url:url, img:MD_IMGS+'thebrain-32.png'};
      data.onclick= function onclick(event) {
        unused(event);
        //noinspection JSUnresolvedVariable
        location.href='aip://thoughtuid/'+encodeURIComponent(ini_data.thought_uid);
      };
      createPopupRow(popup,data);
    }else{
      const data={name:'No thought_uid', url:url, img:MD_IMGS+'error-16.png'};
      data.onclick=function onclick(event){
        unused(event);
        location.href='aip://slickedit/'+encodeURIComponent(disk_path+'\\Desktop.ini');
      };
      createPopupRow(popup,data);
    }
  }else{
    const data={name:'No data', url:url, img:MD_IMGS+'error-16.png'};
    data.onclick=function onclick(event){unused(event);notyDataError('no DESKTOPINI data for '+name0+' '+disk_path);};
    createPopupRow(popup,data);
  }
}

unused(resetPopupForReference);
function resetPopupForReference() {
  clearLinkPopupContent();
  const popup = document.getElementById("linkPopupMenuTable");
  // file references
  for (let i = 0 ; i < fileLinks.length ; i++) {
    const fileNameUrl = fileLinks[i].split("*");
    const name = fileNameUrl[0];
    const url = fileNameUrl[1];
    const ext=fileExt(name);
    //var row = popup.insertRow(popup.rows.length)
    //var imgPopupCell = row.insertCell(0);
    const imageUrl=popupRowFileRefImage(name, url);
    const data={name:name, url:url, img:imageUrl};
    if (ext==='$explhere') {
      createExplhereRows(popup, name,url);
    }else{
      data.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination));};
      createPopupRow(popup,data);
    }
  }
  // folder reference
  for (let i = 0 ; i < folderLinks.length ; i++) {
    const folderNameUrl = folderLinks[i].split("*");
    const name = folderNameUrl[0];
    const url = folderNameUrl[1];
    const row = popup.insertRow(popup.rows.length);
    const imgPopupCell = row.insertCell(0);
    const img='../images/icons/FolderReference.png';
    //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(../images/icons/FolderReference.png) !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../images/icons/FolderReference.png');\"></div>&nbsp;"+name;
    imgPopupCell.innerHTML=popupRowHtml(name, img);
    imgPopupCell.valign="middle";
    imgPopupCell.destination=url;
    imgPopupCell.className="PopupMenuRowDeselected";
    imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
    imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
    imgPopupCell.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination)) };
  }
  // url reference
  for (let i = 0 ; i < urlLinks.length ; i++) {
    const row = popup.insertRow(popup.rows.length);
    const imgPopupCell = row.insertCell(0);
    const destination = urlLinks[i][0];
    let name = urlLinks[i][1];
    if (name === null || name === ''){
      name = destination;
    }
    const img='../images/icons/UrlReference.png';
    //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(../images/icons/UrlReference.png) !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='../images/icons/UrlReference.png');\"></div>&nbsp;"+name;
    imgPopupCell.innerHTML=popupRowHtml(name, img);
    imgPopupCell.valign="middle";
    imgPopupCell.destination=destination;
    imgPopupCell.className="PopupMenuRowDeselected";
    imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
    imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
    imgPopupCell.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination)) };
  }
  // diagram reference
  for (let j = 0 ; j < diagramLinks.length ; j++) {
    const diagramUrlNameType = diagramLinks[j].split("/");
    const url = diagramUrlNameType[0];
    const name = diagramUrlNameType[1];
    const type = diagramUrlNameType[2];
    const img = '../images/icons/' + type + '.png';
    const row = popup.insertRow(popup.rows.length);
    const imgPopupCell = row.insertCell(0);
    //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(" + imgSrc + ") !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgSrc + "');\"></div>&nbsp;"+name
    imgPopupCell.innerHTML=popupRowHtml(name, img);
    imgPopupCell.valign="middle";
    imgPopupCell.className="PopupMenuRowDeselected";
    imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
    imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
    if (url === 'vplink') {
      imgPopupCell.destination= diagramUrlNameType[3].replace('@','/');
      imgPopupCell.vpLinkWithName= diagramUrlNameType[4].replace('@','/');
      imgPopupCell.onclick= function onclick(event) {unused(event); showVpLink(destinationHook(this.destination), this.vpLinkWithName, this) };
    } else {
      imgPopupCell.destination=url;
      if (url !== null && url !== '') {
        imgPopupCell.onclick= function onclick(event) {unused(event); window.open(destinationHook(this.destination),'_self') };
      }
    }
  }
  // shape reference
  for (let j = 0 ; j < shapeLinks.length ; j++) {
    const shapeUrlNameType = shapeLinks[j].split("/");
    const url = shapeUrlNameType[0];
    const name = shapeUrlNameType[1];
    const type = shapeUrlNameType[2];
    const img = '../images/icons/'+type+'.png';
    const row = popup.insertRow(popup.rows.length);
    const imgPopupCell = row.insertCell(0);
    //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(" + imgSrc + ") !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgSrc + "');\"></div>&nbsp;"+name
    imgPopupCell.innerHTML=popupRowHtml(name, img);
    imgPopupCell.valign="middle";
    imgPopupCell.destination=url;
    imgPopupCell.className="PopupMenuRowDeselected";
    imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
    imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
    if (type.length > 0){
      imgPopupCell.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination),'_self') };
    }
  }
  // model element reference
  for (let j = 0 ; j < modelElementLinks.length ; j++) {
    const modelElementUrlNameType = modelElementLinks[j].split("/");
    const url =  modelElementUrlNameType[0];
    const name = modelElementUrlNameType[1];
    const type =  modelElementUrlNameType[2];
    const img = '../images/icons/'+type+'.png';
    const row = popup.insertRow(popup.rows.length);
    const imgPopupCell = row.insertCell(0);
    //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(" + imgSrc + ") !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgSrc + "');\"></div>&nbsp;"+name
    imgPopupCell.innerHTML=popupRowHtml(name, img);
    imgPopupCell.valign="middle";
    imgPopupCell.destination=url;
    imgPopupCell.className="PopupMenuRowDeselected";
    imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
    imgPopupCell.onmouseout= function onmouseover(event) {unused(event); this.className="PopupMenuRowDeselected"; };
    if (type.length > 0){
      imgPopupCell.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination),'_self') };
    }
  }
}

unused(resetPopupForSubdiagram);
function resetPopupForSubdiagram() {
  clearLinkPopupContent();
  const popup = document.getElementById("linkPopupMenuTable");
  // subdiagram
  for (let j = 0 ; j < subdiagramLinks.length ; j++) {
    const diagramUrlNameType = subdiagramLinks[j].split("/");
    const url = diagramUrlNameType[0];
    const name = diagramUrlNameType[1];
    const type = diagramUrlNameType[2];
    const img = '../images/icons/'+type+'.png';
    const row = popup.insertRow(popup.rows.length);
    const imgPopupCell = row.insertCell(0);
    //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(" + imgSrc + ") !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgSrc + "');\"></div>&nbsp;"+name
    imgPopupCell.innerHTML=popupRowHtml(name, img);
    imgPopupCell.valign="middle";
    imgPopupCell.destination=url;
    imgPopupCell.className="PopupMenuRowDeselected";
    imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
    imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
    if (url !== null && url !== '') {
      imgPopupCell.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination),'_self') };
    }
  }
}

unused(movePopupPositionToReferenceIconPosition);
function movePopupPositionToReferenceIconPosition() {
  movePopupPositionToSpecificPosition(xForRefIcon, yForRefIcon);
}

unused(movePopupPositionToSubdiagramIconPosition);
function movePopupPositionToSubdiagramIconPosition() {
  movePopupPositionToSpecificPosition(xForSubDiagramIcon, yForSubDiagramIcon);
}

unused(movePopupPositionToCursorPosition);
function movePopupPositionToCursorPosition(event) {
  const diagram = document.getElementById("diagram");
  const e = (window.event) ? window.event : event;
  let xOffset = e.clientX;
  let yOffset = e.clientY;
  if (document.all){
    if (!document.documentElement.scrollLeft){
      xOffset += document.body.scrollLeft;
    }else{
      xOffset += document.documentElement.scrollLeft;
    }
    if (!document.documentElement.scrollTop){
      yOffset += document.body.scrollTop;
    }else{
      yOffset += document.documentElement.scrollTop;
    }
  }else{
    xOffset += window.pageXOffset;
    yOffset += window.pageYOffset;
  }
  movePopupPositionToSpecificPosition(xOffset, yOffset);
}

function movePopupPositionToSpecificPosition(x, y) {
  const popupLayer = document.getElementById("linkPopupMenuLayer");
  const N = (document.all) ? 0 : 1;
  if (N) {
    popupLayer.style.left = x;
    popupLayer.style.top = y;
  } else {
    popupLayer.style.posLeft = x;
    popupLayer.style.posTop = y;
  }
}

unused(switchPopupShowHideStatus);
function switchPopupShowHideStatus(){
  const popup = document.getElementById("linkPopupMenuTable");
  if (popup.style.visibility === "visible") {
    hideLinkPopup();
  }else{
    showLinkPopup();
  }
}

function adjustPopupPositionForSpotLightTable() {
  movePopupPositionToSpecificPosition(cursorX,cursorY);
}

function showLinkPopup(){
  const popup = document.getElementById("linkPopupMenuTable");
  if (popup) {
    popup.style.visibility="visible";
    document.getElementById("linkPopupMenuLayer").style.visibility="visible";
    document.getElementById("linkPopupMenuLayer").style.zIndex=10;
  }
}

function hideLinkPopup(){//
  const popup = document.getElementById("linkPopupMenuTable");
  if (popup) {
    popup.style.visibility="hidden";
    document.getElementById("linkPopupMenuLayer").style.visibility="hidden";
  }
}

function clearLinkPopupContent(){
  const popup = document.getElementById("linkPopupMenuTable");
  if (popup) {
    for (let i = popup.rows.length ; i >0 ; i--) {
      popup.deleteRow(0);
    }
  }
}

unused(movePopupPositionToTransitorIconPosition);
function movePopupPositionToTransitorIconPosition() {
  movePopupPositionToSpecificPosition(xForTransIcon, yForTransIcon);
}

function resetPopupForTransitor() {
  clearLinkPopupContent();
  const popup = document.getElementById("linkPopupMenuTable");
  // transitor
  let row = popup.insertRow(popup.rows.length);
  let popupCell = row.insertCell(0);
  popupCell.innerHTML="<div style=\"font-size:11px\">From:</div>";
  for (let j = 0 ; j < fromTransitorLinks.length ; j++) {
    const shapeUrlNameType = fromTransitorLinks[j].split("/");
    addPopupItem(popup, shapeUrlNameType);
  }
  row = popup.insertRow(popup.rows.length);
  popupCell = row.insertCell(0);
  popupCell.innerHTML="<div style=\"font-size:11px\">To:</div>";
  for (let j = 0 ; j < toTransitorLinks.length ; j++) {
    const shapeUrlNameType = toTransitorLinks[j].split("/");
    addPopupItem(popup, shapeUrlNameType);
  }
}

function addPopupItem(popup, shapeUrlNameType) {
  const url = shapeUrlNameType[0];
  const name = shapeUrlNameType[1];
  const type = shapeUrlNameType[2];
  const img = '../images/icons/'+type+'.png';
  const row = popup.insertRow(popup.rows.length);
  const imgPopupCell = row.insertCell(0);
  //imgPopupCell.innerHTML="<div style=\"float: left; width: 18px !important;height: 18px !important;background-image:url(" + imgSrc + ") !important; background-image:url(''); filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + imgSrc + "');\"></div>&nbsp;"+name
  imgPopupCell.innerHTML=popupRowHtml(name, img);
  imgPopupCell.valign="middle";
  imgPopupCell.destination=url;
  imgPopupCell.className="PopupMenuRowDeselected";
  imgPopupCell.onmouseover= function onmouseover(event) {unused(event);this.className="PopupMenuRowSelected"; };
  imgPopupCell.onmouseout= function onmouseover(event) {unused(event);this.className="PopupMenuRowDeselected"; };
  imgPopupCell.onclick= function onclick(event) {unused(event);window.open(destinationHook(this.destination),'_self') };
}

function showVpLink(link, linkWithName, linkElem){
  // getting absolute location in page
  let lLeft = 0;
  let lTop = 0;
  let lParent = linkElem;
  while (lParent !== null) {
    lLeft += lParent.offsetLeft;
    lTop += lParent.offsetTop;
    lParent = lParent.offsetParent;
  }
  showVpLinkAt(link, linkWithName, lLeft, lTop + linkElem.offsetHeight);
}

unused(showVpLinkAtDiagram);
function showVpLinkAtDiagram(link, linkWithName, aLeft, aTop){
  let lLeft = 0;
  let lTop = 0;
  let lParent = document.getElementById('diagram');
  while (lParent !== null) {
    lLeft += lParent.offsetLeft;
    lTop += lParent.offsetTop;
    lParent = lParent.offsetParent;
  }
  showVpLinkAt(link, linkWithName, lLeft + aLeft, lTop + aTop);
}

function showVpLinkAt(link, linkWithName, aLeft, aTop){
  const popup = document.getElementById("vplink");
  if (popup.style.visibility === "visible") {
    popup.style.visibility="hidden";
  } else {
    const withName = document.getElementById("vplink-checkbox");
    const linkText = document.getElementById("vplink-text");
    if (withName.checked) {
      linkText.value = linkWithName;
      vpLinkNameToggle = link;
    } else {
      linkText.value = link;
      vpLinkNameToggle = linkWithName;
    }
    const N = (document.all) ? 0 : 1;
    if (N) {
      popup.style.left = aLeft;
      popup.style.top = aTop;
    } else {
      popup.style.posLeft = aLeft;
      popup.style.posTop = aTop;
    }
    popup.style.visibility="visible";
    linkText.focus();
    linkText.select();
  }
}

unused(vpLinkToggleName);
function vpLinkToggleName() {
  const linkText = document.getElementById("vplink-text");
  const tmp = linkText.value;
  linkText.value = vpLinkNameToggle;
  vpLinkNameToggle = tmp;
  linkText.focus();
  linkText.select();
}