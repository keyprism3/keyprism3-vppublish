## RMF-publish

`C:\_\keyprism3\._vphtml\publish`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `6`
- `webthought_id` - `6`
- `thought_uid` - `9F9E1571-D044-9BE9-88AF-C8944D045F82`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/A4htPVqGAqAAAh17`
- `FmColorName` - `red`
- `ColorHex` - `#F56C4B`
- `ColorRgb` - `245,108,75`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cpublish) _Visual Paradigm Report_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%26webthought_id%3D%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FggwpPVqGAqAAAh%30%34) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cpublish%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FA%34htPVqGAqAAAh%31%37) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%39F%39E%31%35%37%31-D%30%34%34-%39BE%39-%38%38AF-C%38%39%34%34D%30%34%35F%38%32) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/._vphtml/publish/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-6) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `content`
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `images`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cpublish%5Creferences%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cpublish%5Creferences%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) `references` - KeyPrism3 Project
- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `voices`

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cpublish%5Cindex.html) `index.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_blank-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cpublish%5Ckeyprism%33.xml) `keyprism3.xml` - Brain output, TBI

