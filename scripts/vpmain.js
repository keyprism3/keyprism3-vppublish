console.log('VPMAIN window:'+window.name);

$(function(){
  console.log('VPMAIN jquery Loaded for:'+window.name);
  var bb='https://bitbucket.org/'+BITBUCKET_PRJ+'/'+BITBUCKET_REPO+'/src/master/';
  var geshi='http://mrobbinsassoc.com/tools/geshi/geshi-url-lang-title.php?url=';
  var refpfx='http%3A//mrobbinsassoc.com/sn/vp/publish/references/';
  //http%3A//mrobbinsassoc.com/sn/vp/publish/references/dashboard.jsx.js&lang=javascript'
  //RMF-_docs.md
  $('font[color="#000000"]').attr('color',null);
  $('font[color="rgb(68, 68, 68)"]')
    .attr('color',null)
    .attr('face',null)
    .attr('style',null)
    .attr('size',null)
    .attr('class','md-code');
  // <font color="rgb(65, 131, 196)" face="Courier New" size="12px" style="font-size:12px;">CI
  //     BUILD of socialnitro-app</font>
  $('font[color="rgb(65, 131, 196)"]')
  .attr('color',null)
  .attr('face',null)
  .attr('style',null)
  .attr('size',null);
  //<font face="Courier New" size="12px" style="font-size:12px;">Project
  //    Documentation -<span class="Apple-converted-space">&nbsp;</span></font>
  $('font[face="Courier New"][size="12px"]')
  .attr('color',null)
  .attr('face',null)
  .attr('style',null)
  .attr('size',null);
  $('font[face="Courier New"]')
  .attr('face',null);


  $('a[href^="http://localhost/aip.html"').attr('href',null);//.css('cursor','pointer');
  //http://localhost/aip.html?cmd=open/C%3A%5C_%5Csocialnitro-app%5Cclient%5Clib
  //hideLinkPopup
  $('body').click(function(e){
    console.log(e);
    var tgt=$(e.target);
    if (!tgt.attr('onclick')) {
      hideLinkPopup();
    }
  });
  $('.ItemLink').each(function(){
    var me=$(this);
    var hr=me.attr('href');
    if (hr.indexOf('../references/')===0) {
      var f=hr.substr('../references/'.length);
      var fa=f.split('.');
      var ext=fa[fa.length-1];
      if (ext==='md') {
        var ff=f.replace(/\.md/g,'').substr('RMF-'.length);
        ff=ff.replace(/-/g,'/');
        var fnew=ff+'/'+f;
        hr=bb+fnew;
        me.attr('href',hr);
      }else if (ext==='$explhere') {
        var ra=f.split('`');
        ra=ra.reverse();
        var pfx='https://bitbucket.org/'+BITBUCKET_PRJ+'/';
        pfx=pfx+ra[1]+'/src/master';
        var x;
        for (x=2;x<ra.length;x++) {
          pfx+='/'+ra[x];
        }
        hr=pfx;
        me.attr('href',hr);
      }else if (ext==='feature') {
        var url=bb+'tests/chimp/features/'+f;
        me.attr('href',url);
      }else{
        hr=geshi+refpfx+f;
        if (ext==='js'||ext==='jsx'||ext==='json') {
          hr+='&lang=javascript';
        }
        if (ext==='less'||ext==='css') {
          hr+='&lang=css';
        }
        if (ext==='html') {
          hr+='&lang=HTML5';
        }
        if (ext==='txt') {
          hr+='&lang=Plaintext';
        }
        hr+='&title='+escape(f);
        me.attr('href',hr);
      }
      //Plaintext
    }
  });
});

