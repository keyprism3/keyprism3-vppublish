## README

`C:\_\keyprism3\._vphtml\scripts`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `7`
- `webthought_id` - `7`
- `thought_uid` - `2F80368E-7E44-DD91-A81B-0600AB2B356C`
- `vp_url` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/5CFtPVqGAqAAAh2F`
- `FmColorName` - `purple`
- `ColorHex` - `#C195E4`
- `ColorRgb` - `193,149,228`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cscripts) _Auxillary Scripts for VP_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%35%26webthought_id%3D%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FggwpPVqGAqAAAh%30%34) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cscripts%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F%35CFtPVqGAqAAAh%32F) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%32F%38%30%33%36%38E-%37E%34%34-DD%39%31-A%38%31B-%30%36%30%30AB%32B%33%35%36C) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3/src/master/._vphtml/scripts/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-7) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_YcUAPVqGAqAAAhK3.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._vphtml%5Cscripts%5Cvpmain.js) `vpmain.js`

